# https://www.chezmoi.io/
# chezmoi config file:
# ~/.config/chezmoi/chezmoi.toml

export EDITOR=nvim

USER=``

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
# ZSH_THEME="robbyrussell"
ZSH_THEME="agnoster"

# set these to hide username on the command line
DEFAULT_USER="dstevens"

HIST_STAMPS="yyyy-mm-dd"

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git
)

# For a full list of active aliases, run `alias`.

if [ -f "$HOME/.aliases" ]; then
  . $HOME/.aliases
fi

export TODOTXT_DEFAULT_ACTION=ls
export HISTCONTROL=ignorespace


KEYTIMEOUT=2
export ZSH="/home/dstevens/.oh-my-zsh"
export LC_ALL=en_US.UTF-8



source $ZSH/oh-my-zsh.sh

# requests cheat.sh for cheatsheet docs on command from first passed parameter
cht() {
  var=$(curl -f -s -S -k cheat.sh/$@)
  printf "%s \n" "$var"
}

procport() {
  var=$(lsof -n -i :$@)
  printf "%s \n" "$var"
}

bak() {
  bak_str=".bak"
  command cp -i "$@" "$@$bak_str"
}

# adds new line to prompt
prompt_end() {
  if [[ -n $CURRENT_BG ]]; then
      print -n "%{%k%F{$CURRENT_BG}%}$SEGMENT_SEPARATOR"
  else
      print -n "%{%k%}"
  fi

  print -n "%{%f%}"
  CURRENT_BG=''

  printf "\n»";
}

if [[ -z $NODE_ENV ]]; then
    export NODE_ENV=dev
fi

feh() {
	qlmanage -p "$@"
}

# for https://github.com/so-fancy/diff-so-fancy
git config --global core.pager "diff-so-fancy | less --tabs=4 -RFX"

# bash completion for utt time tracker: https://github.com/larose/utt
complete -o nospace -o default -o bashdefault -F _python_argcomplete utt

# Run something, muting output or redirecting it to the debug stream
# depending on the value of _ARC_DEBUG.
__python_argcomplete_run() {
    if [[ -z "$_ARC_DEBUG" ]]; then
        "$@" 8>&1 9>&2 1>/dev/null 2>&1
    else
        "$@" 8>&1 9>&2 1>&9 2>&1
    fi
}

_python_argcomplete() {
    local IFS=$'\013'
    local SUPPRESS_SPACE=0
    if compopt +o nospace 2> /dev/null; then
        SUPPRESS_SPACE=1
    fi
    COMPREPLY=( $(IFS="$IFS" \
                  COMP_LINE="$COMP_LINE" \
                  COMP_POINT="$COMP_POINT" \
                  COMP_TYPE="$COMP_TYPE" \
                  _ARGCOMPLETE_COMP_WORDBREAKS="$COMP_WORDBREAKS" \
                  _ARGCOMPLETE=1 \
                  _ARGCOMPLETE_SUPPRESS_SPACE=$SUPPRESS_SPACE \
                  __python_argcomplete_run "$1") )
    if [[ $? != 0 ]]; then
        unset COMPREPLY
    elif [[ $SUPPRESS_SPACE == 1 ]] && [[ "$COMPREPLY" =~ [=/:]$ ]]; then
        compopt -o nospace
    fi
}

autoload -Uz compinit && compinit

# https://stackoverflow.com/questions/38558989/node-js-heap-out-of-memory
export NODE_OPTIONS=--max_old_space_size=4096

eval "$(zoxide init zsh)"

export PYENV_ROOT=`pyenv root`

pan() { pandoc "$1" | lynx -stdin; }

# fnm node version manager
eval "$(fnm env --use-on-cd)"

# go
export PATH=$PATH:/usr/local/go/bin

# cli tool to generate random UUID
# > uuidgen

# local bin
export PATH=/home/dstevens/.local/bin:$PATH
# suralink

# replace all remotes to a new org
# find . -name ".git" -exec sed -i 's/theRealSuralink/suralink/g' {}/config \;

unsealVault() {
  echo "unsealing vault"
  docker exec shared_services_vault_1 vault operator unseal /CGJd0ntaxaldPB9wtvzKTZJOeUEjKUbqCsDUKzjv1o=
}


[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
